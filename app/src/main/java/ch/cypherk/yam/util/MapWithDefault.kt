package ch.cypherk.yam.util

/**Default map classes return a [V?] type, even when they have a default key.
   this changes that.*/
class MapWithDefault<K,V>(val map:Map<K,V>,val default:(K)->V): Map<K,V> by map{
    override fun get(key: K): V = map[key] ?: default(key)
}

class MutableMapWithDefault<K,V>(val map:MutableMap<K,V>,val default:(K)->V): MutableMap<K,V> by map{
    override fun get(key: K): V = map[key] ?: default(key)
}

/**override standard [Map.withDefault]*/
fun <K,V>Map<K,V>.withDefault(default:(K)->V) =  MapWithDefault(this,default)
fun <K,V>MutableMap<K,V>.withDefault(default:(K)->V) =  MutableMapWithDefault(this,default)
