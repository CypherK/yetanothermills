package ch.cypherk.yam.domain.model.game.statechanges

/**Encapsulates a change in the game state that may be of interest to 3rd parties, e.g. to the View*/
interface StateChange