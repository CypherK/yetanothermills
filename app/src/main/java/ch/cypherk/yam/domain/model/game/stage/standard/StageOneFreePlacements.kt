package ch.cypherk.yam.domain.model.game.stage.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.DummyRules
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.rules.standard.PlayersCanPlaceFreely
import ch.cypherk.yam.domain.model.game.stage.GameHasEnded
import ch.cypherk.yam.domain.model.game.stage.Stage
import ch.cypherk.yam.domain.model.game.statechanges.NewTurn
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor
import ch.cypherk.yam.util.withDefault
import java.lang.IllegalArgumentException

/**Each player gets 9 stones to place at free locations. If a player manages to close a mill, he
 * gets to remove a stone of his opponent's.*/
class StageOneFreePlacements(
        communicator: GuiActor,
        players:Set<Player>,
        board: Mills,
        rules: Rules = PlayersCanPlaceFreely(board)
):StandardGameStage(communicator,players,board,rules){
    private val availableStones = players.associate { Pair(it,9) }.toMutableMap()
            .withDefault {throw IllegalArgumentException("unknown player ${it.name}")}

    fun allStonesPlaced():Boolean{
        for(a in availableStones.values)
            if(a > 0) return false
        return true
    }

    override fun playNextTurn(player: Player): Stage {
        broadcast(NewTurn(player.conceptualPlayer))
        broadcast("${availableStones[player]} stones left to place")

        //place a new stone
        val pos = placeNewStone(player)

        /*note to self: [--] and [-=] will result in
        * java.lang.IllegalAccessError: Method 'void kotlin.collections.MapsKt__MapsKt.set(java.util.Map,
        * java.lang.Object, java.lang.Object)' is inaccessible to class ch.cypherk.yam.[...]*/
        availableStones[player] = availableStones[player] - 1

        return if(allStonesPlaced()) StageTwoMoveExisting(communicator,players,board) else this
    }
}