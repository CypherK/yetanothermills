package ch.cypherk.yam.domain.model.base

import ch.cypherk.yam.domain.model.base.MovableDirection.Companion.oppositeMovableDirection
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.util.withDefault

/** encapsulates positions on the board*/
class Node(val conceptualPosition: ConceptualPosition){
    var owner: PositionOwner = NOBODY
    override fun toString() = "${conceptualPosition}${if (owner == NOBODY) "" else "(${owner.name})"}"

    val connection = mutableMapOf<MovableDirection, Edge>().withDefault { Edge.None }
    fun connectedNodes() = connection.values.filter { it is Edge.To }.map { (it as Edge.To).p }

    /**Connects this [Node] to another and it, in return, to this.
     * In so doing, overwrites this [Node]'s [connection] value for the [fromMyPOV] direction
     * and in the [target], overwrites its [connection] value for the [oppositeMovableDirection] of [fromMyPOV]
     * @throws IllegalArgumentException if the [MovableDirection] [fromMyPOV] has no [oppositeMovableDirection]*/
    fun connectBothWaysWith(target: Node, fromMyPOV: MovableDirection){
        this.connection[fromMyPOV] = Edge.To(target)
        oppositeMovableDirection[fromMyPOV]?.let { oppositeDirection ->
            target.connection[oppositeDirection] = Edge.To(this)
        }?: throw IllegalArgumentException("cannot connect bidirectionally because '${fromMyPOV.name}' has no opposite movable direction")
    }
}