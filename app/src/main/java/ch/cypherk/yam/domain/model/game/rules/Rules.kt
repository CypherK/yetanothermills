package ch.cypherk.yam.domain.model.game.rules

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.mills.Mills

interface Rules{
    /** the [Mills] instance to which the [Rules] are applied*/
    val board:Mills

    /**@return true if there is any move at all that the player can make, false otherwise*/
    fun hasValidMove(player:Player):Boolean

    /**@return all valid moves for [player] when moving from the [from] position*/
    fun validMoves(player:Player,from:ConceptualPosition):Set<ConceptualPosition>

    /**@return true if [player] can move a stone from [from] to [to], false otherwise*/
    fun isValidMove(player:Player,from:ConceptualPosition,to:ConceptualPosition):Answer

    /**@return true if [player] can remove the stone at [pos], false otherwise*/
    fun playerCanRemove(player:Player, pos:ConceptualPosition):Answer

    /**@return all positions owned by [player] that form closed mills*/
    fun closedMills(player: Player):Set<ConceptualPosition>

    /**@return all [ConceptualPosition]s that form a closed mill with [pos] (including [pos])
     * if any such exist or an empty set if [pos] is not part of any closed mills*/
    fun closedMillsAt(pos: ConceptualPosition):Set<ConceptualPosition>
}