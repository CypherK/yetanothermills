package ch.cypherk.yam.domain.model.game.rules.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Answer
import ch.cypherk.yam.domain.model.game.rules.Answer.*
import ch.cypherk.yam.domain.model.mills.Mills

/**Players select a stone, then move it to an adjacent, free position
 * A player that has only three stones left can "jump", i.e. move to any free position*/
class PlayersMoveToAdjacent(board: Mills):StandardRules(board){

    override fun hasValidMove(player: Player): Boolean {
        val nofCandidates = player.nofStones

        //a player with less than three stones can no longer close a mill and has lost
        if (nofCandidates < 3) return false

        //a player with exactly three stones can jump, so he can always move
        if (nofCandidates == 3) return true

        //a player with more than 3 stones must be able to move a stone to an unoccupied position
        val owner = player.conceptualPlayer
        for (ownedNode in board.position.values.filter { it.owner == owner }){
            for(reachable in ownedNode.connectedNodes()){
                if(reachable.owner == NOBODY) return true
            }
        }
        return false
    }

    override fun validMoves(player: Player, from: ConceptualPosition): Set<ConceptualPosition> {
        val nFrom = board.position[from]
        if(player.nofStones < 4) return nFrom.connectedNodes().map { it.conceptualPosition }.toSet()
        else return board.position.values.filter { it.owner == NOBODY }.map { it.conceptualPosition }.toSet()
    }

    override fun isValidMove(player: Player, from: ConceptualPosition, to: ConceptualPosition): Answer {
        val nFrom = board.position[from]
        val nTo = board.position[to]

        if(nFrom.owner != player.conceptualPlayer) return No(CannotMoveOthersStones)
        if(nTo.owner != NOBODY) return No(PositionNotFree)
        if(player.nofStones > 3 && !nFrom.connectedNodes().contains(nTo)) return No(CannotJumpYet)

        return Yes
    }
}