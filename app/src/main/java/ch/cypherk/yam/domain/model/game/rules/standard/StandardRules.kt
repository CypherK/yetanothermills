package ch.cypherk.yam.domain.model.game.rules.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Answer.*
import ch.cypherk.yam.domain.model.game.rules.Answer
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.rules.Reason
import ch.cypherk.yam.domain.model.mills.Mills

/**Does not implement [hasValidMove], [validMoves] and [isValidMove]*/
abstract class StandardRules(override val board:Mills): Rules {
    /*reasons for failure*/
    object CannotRemoveOwn: Reason{override val resolution = "you cannot remove your own stones"}
    object PositionNotFree: Reason{override val resolution = "stones must be placed on free positions"}
    object MustRemoveNonMillsFirst:Reason{override val resolution = "you cannot remove stones that are part of a closed mills if there are still other stones you can take"}
    object CannotMoveOthersStones:Reason{override val resolution = "you can only move your own stones"}
    object CannotJumpYet:Reason{override val resolution = "if you have more than 3 stones, you can only move to adjacent positions"}

    private val millsVisitor = MillsVisitor()

    /**Any mills must include at least one of the middle positions
     * A1 - - -  A2 - - - A3
     *  |        |         |
     *  |  B1 -- B2 -- B3  |
     *  |  |     |      |  |
     *  |  |  C1-C2-C3  |  |
     *  A4-B4-C4    C5-B5-A5
     *  |  |  C6-C7-C8  |  |
     *  |  |     |      |  |
     *  |  B6 -- B7 -- B8  |
     *  |        |         |
     *  A6 - - - A7 - - - A8
     * */
    companion object {
        val positionsToCheck = setOf(
                A2,B2,C2,
                A4,B4,C4,
                C5,B5,A5,
                C7,B7,A7
        )
    }

    override fun closedMills(player: Player): Set<ConceptualPosition> = closedMills(player.conceptualPlayer)
    override fun closedMillsAt(pos: ConceptualPosition): Set<ConceptualPosition> = millsVisitor.mills(board.position[pos])

    override fun playerCanRemove(player: Player, pos: ConceptualPosition): Answer = playerCanRemove(player.conceptualPlayer,pos)

    protected fun playerCanRemove(player: PositionOwner, pos: ConceptualPosition):Answer {
        val owner = board.position[pos].owner

        //if there is no stone or [player] owns the position, he cannot remove
        if(owner == NOBODY || owner == player) return No(CannotRemoveOwn)

        //if [pos] is part of a closed mill and there are other stones to take he must take a different stone
        if(!closedMillsAt(pos).isEmpty()){
            val allOpponentStones = board.position.values.filter { it.owner == owner}.map { it.conceptualPosition }
            val opponentStonesInMills = closedMills(owner)

            if(allOpponentStones.size > opponentStonesInMills.size) return No(MustRemoveNonMillsFirst)
        }

        return Yes
    }

    /**@return all positions owned by [player] that form closed mills*/
    private fun closedMills(player:PositionOwner):Set<ConceptualPosition>{
        val candidates = positionsToCheck.filter { board.position[it].owner == player }

        val mills = candidates.flatMap { millsVisitor.mills(board.position[it]) }
        return mills.toSet()
    }
}