package ch.cypherk.yam.domain.model.game.player

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner

interface Player{
    /**an identifier*/
    val name:String

    /**a unique identifier that is used to communicate with the model*/
    val conceptualPlayer : PositionOwner

    /**The amount of stones the player has left on the board*/
    var nofStones:Int

    fun selectPosition():ConceptualPosition
}