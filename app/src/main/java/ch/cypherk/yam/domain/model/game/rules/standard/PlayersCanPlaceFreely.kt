package ch.cypherk.yam.domain.model.game.rules.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Answer
import ch.cypherk.yam.domain.model.game.rules.Answer.*
import ch.cypherk.yam.domain.model.mills.Mills

class PlayersCanPlaceFreely(board:Mills):StandardRules(board){

    /**There are 24 positions in Standard Mills and a total of 18 stones are placed,
     * so a player always has at least one free position to choose
     * @return true*/
    override fun hasValidMove(player: Player): Boolean {
        return true
    }

    override fun validMoves(player: Player, from: ConceptualPosition): Set<ConceptualPosition> {
        return board.position.values.filter { it.owner == NOBODY }.map { it.conceptualPosition }.toSet()
    }

    override fun isValidMove(player: Player, from: ConceptualPosition, to: ConceptualPosition): Answer {
        return if(board.position[to].owner == NOBODY) Yes else No(PositionNotFree)
    }
}