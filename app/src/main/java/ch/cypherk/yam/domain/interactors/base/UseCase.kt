package ch.cypherk.yam.domain.interactors.base
import ch.cypherk.yam.domain.threading.Executor

/** Encapsulates an action. If the [UseCase]'s [invoke] requires arguments, they should be injected via the constructors*/
interface UseCase<out Result,in Params> {
    /**Encapsulates a computational step on the model Should be executed in the background by an [Executor].
     * @param onResult called when the [UseCase] successfully completes, takes the computated result and does whatever the client specifies with it
     * @param onError called when the [UseCase] is unable to return a valid result, provides a [UseCaseFailedException] for the client to handle*/
    suspend operator fun invoke(
            arg:Params? = null,
            onResult:(Result) -> Unit={},
            onError:(reason:UseCaseFailedException)->Unit={}
    )
}