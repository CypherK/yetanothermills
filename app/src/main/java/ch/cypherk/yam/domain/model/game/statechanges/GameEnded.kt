package ch.cypherk.yam.domain.model.game.statechanges

import ch.cypherk.yam.domain.model.base.PositionOwner

sealed class GameEnded:StateChange{
    data class WithWinner(val winner:PositionOwner):GameEnded()
    object WithoutWinner:GameEnded()
}