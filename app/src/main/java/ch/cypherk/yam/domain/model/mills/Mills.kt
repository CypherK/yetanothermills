package ch.cypherk.yam.domain.model.mills

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.Node
import ch.cypherk.yam.util.MapWithDefault

interface Mills{
    val position:MapWithDefault<ConceptualPosition, Node>
}