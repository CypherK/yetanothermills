package ch.cypherk.yam.domain.model.game.rules.standard

import ch.cypherk.yam.domain.model.base.*
import ch.cypherk.yam.domain.model.base.MovableDirection.*
import ch.cypherk.yam.domain.model.base.PositionOwner.*


class MillsVisitor{

    private val nofNodesInAMill = 3

    /**@return a set of node that form closed mills with the [origin] node*/
    fun mills(origin: Node):Set<ConceptualPosition>{
        val owner= origin.owner

        if(owner == NOBODY) return emptySet()

        //check up/down
        val candidateA = nodesWithSameOwner(origin, mutableSetOf(),owner, setOf(UP,DOWN))

        //check left/right
        val candidateB = nodesWithSameOwner(origin, mutableSetOf(),owner, setOf(LEFT,RIGHT))

        return listOf(candidateA, candidateB).filter { it.size == nofNodesInAMill }.flatten().map { it.conceptualPosition }.toSet()
    }

    private fun nodesWithSameOwner(node:Node,visited:MutableSet<Node>,owner:PositionOwner,dirs:Collection<MovableDirection>):Set<Node>{
        visited.add(node)

        return if(node.owner != owner) emptySet()
        else dirs.flatMap {
            val edge = node.connection[it]
            when(edge){
                is Edge.To ->{
                    val next = edge.p
                    if(!visited.contains(next)) nodesWithSameOwner(next,visited,owner,dirs)
                    else emptySet()
                }
                else -> emptySet()
            }
        }.toSet() + node
    }
}