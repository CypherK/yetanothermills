package ch.cypherk.yam.domain.model.game.statechanges

import ch.cypherk.yam.domain.model.base.ConceptualPosition

data class PositionHasBeenSelected(val pos:ConceptualPosition):StateChange