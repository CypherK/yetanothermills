package ch.cypherk.yam.domain.model.game.stage.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Answer
import ch.cypherk.yam.domain.model.game.rules.Answer.*
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.stage.Stage
import ch.cypherk.yam.domain.model.game.statechanges.MillsClosed
import ch.cypherk.yam.domain.model.game.statechanges.OwnerHasChanged
import ch.cypherk.yam.domain.model.game.statechanges.ValidMoveAccepted
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor
import ch.cypherk.yam.util.withDefault
import java.lang.IllegalArgumentException

/**Does not implement [playNextTurn] but provides operations common to all [StandardGameStage]s*/
abstract class StandardGameStage(
        override val communicator: GuiActor,
        override val players:Set<Player>,
        override val board: Mills,
        override val rules: Rules
):Stage{

    /**maps [PositionOwner] to the the [Player] corresponding to it*/
    val playerThatIS by lazy{ players.associate { it.conceptualPlayer to it }
            .withDefault { throw IllegalArgumentException("no player for PositionOwner $it") }
    }

    /**sets the owner of [pos] to [newOwner] and [broadcast]s the change*/
    protected fun setOwner(pos:ConceptualPosition,newOwner: PositionOwner){
        val node = board.position[pos]

        val oldOwner = node.owner
        if(oldOwner != NOBODY) playerThatIS[oldOwner].nofStones--

        node.owner = newOwner
        if(newOwner != NOBODY) playerThatIS[newOwner].nofStones++

        broadcast(OwnerHasChanged(pos,newOwner))
    }

    /**queries the [player] for a position and tries to place a stone there
     * keeps looping until the player provides a position at which a stone could be placed*/
    fun placeNewStone(player:Player):ConceptualPosition{
        var success = false
        var pos = INVALID
        while(!success){
            pos = player.selectPosition()
            val moved = moveStone(player,from=INVALID,to=pos)
            success = when(moved){
                is No -> {
                    broadcastError("You cannot place a stone there: ${moved.reason.resolution}")
                    false
                }
                else -> true
            }
        }
        return pos
    }

    /**removes a stone from the board*/
    protected fun removeStone(at:ConceptualPosition){
        require(at != INVALID){"cannot remove a stone from an INVALID position"}
        setOwner(at,NOBODY)
    }

    /**
     * @param from can be [INVALID] if an addtional stone is placed, rather than an existing one moved
     * @param to must not be [INVALID] use the [removeStone] function to remove a stone, instead
     * @return true if move successful, false otherwise*/
    protected fun moveStone(player:Player,from:ConceptualPosition, to:ConceptualPosition):Answer{
        require(to != INVALID){"cannot move to INVALID position"}

        val canMove = rules.isValidMove(player,from,to)
        if(canMove is No) return canMove
        else{
            broadcast(ValidMoveAccepted())

            //remove the stone from the old position (if there is an old position)
            if(from != INVALID) removeStone(from)

            //and place it at the new position
            setOwner(to,player.conceptualPlayer)

            checkForMills(player,to)

            return Yes
        }
    }

    /**checks if there are closed mills formed with [pos] and if so,
     * broadcasts them and enters a loop that lets the player remove an opponent's stone */
    protected fun checkForMills(player:Player, pos:ConceptualPosition){
        require(board.position[pos].owner == player.conceptualPlayer){
            "player ${player.conceptualPlayer} does not own position $pos"
        }

        val closedMills = rules.closedMillsAt(pos)

        if(!closedMills.isEmpty()){
            //let the user know
            broadcast(MillsClosed(closedMills))

            //now wait for him to select a stone to remove
            var success = false
            var pos = INVALID
            while(!success){
                pos = player.selectPosition()

                val removable = rules.playerCanRemove(player,pos)

                success = when(removable){
                    is No -> {
                        broadcastError(removable.reason.resolution)
                        false
                    }
                    else -> true
                }
            }

            //and remove the stone
            removeStone(pos)
        }
    }
}