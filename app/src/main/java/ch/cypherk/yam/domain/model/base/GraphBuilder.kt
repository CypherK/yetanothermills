package ch.cypherk.yam.domain.model.base

import ch.cypherk.yam.domain.model.base.MovableDirection.*
import ch.cypherk.yam.util.withDefault


object GraphBuilder{
    private val initMap = mapOf<ConceptualPosition,Node>()
            .withDefault { throw IllegalStateException("the map in GraphBuilder has not been updated") }
    var positionToNode = initMap
    fun reset(){positionToNode = initMap}
}

fun ConceptualPosition.connectPositions(
        dir: MovableDirection,
        target: ConceptualPosition,
        bidirectional:Boolean
){
    val n = GraphBuilder.positionToNode
    if(bidirectional) n[this].connectBothWaysWith(n[target],dir)
    else throw IllegalArgumentException("unidirectional connections not implemented")
}

fun ConceptualPosition.up(target: ConceptualPosition, bidirectional:Boolean=true) = connectPositions(UP,target,bidirectional)
fun ConceptualPosition.down(target: ConceptualPosition, bidirectional:Boolean=true) = connectPositions(DOWN,target,bidirectional)
fun ConceptualPosition.left(target: ConceptualPosition, bidirectional:Boolean=true) = connectPositions(LEFT,target,bidirectional)
fun ConceptualPosition.right(target: ConceptualPosition, bidirectional:Boolean=true) = connectPositions(RIGHT,target,bidirectional)