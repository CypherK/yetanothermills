package ch.cypherk.yam.domain.model.game.statechanges

sealed class Message:StateChange {
    data class Info(val msg: String):Message()
    data class Error(val msg:String):Message()
}