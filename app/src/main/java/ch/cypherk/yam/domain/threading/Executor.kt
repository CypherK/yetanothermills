package ch.cypherk.yam.domain.threading
import ch.cypherk.yam.domain.interactors.base.UseCase
import ch.cypherk.yam.domain.interactors.base.UseCaseFailedException

/**Handles the actual execution of a [UseCase].*/
interface Executor {
    /**Takes a [UseCase] and makes sure it's executed in an asynchronous manner ("in the background")*/
    operator fun <Result,Params>invoke(
            u: UseCase<Result,Params>,
            arg:Params? = null,
            onResult:(Result) -> Unit={},
            onError:(reason: UseCaseFailedException)->Unit={}
    )
}