package ch.cypherk.yam.domain.model.ui

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.game.statechanges.StateChange
import kotlinx.coroutines.channels.ReceiveChannel

interface GuiActor{

    /** receives a [StateChange] and broadcasts it to all listening clients*/
    suspend fun broadcastStateChange(change:StateChange)

    /** subscribes to [StateChange]s broadcasted by [broadcastStateChange]*/
    suspend fun subscribeToStateChanges():ReceiveChannel<StateChange>

    /** Receives a [ConceptualPosition] that may or may not get accepted and acted upon.*/
    suspend fun offerUserSelectedPosition(conceptualPosition: ConceptualPosition)

    /** waits for a [ConceptualPosition] to be offered via [offerUserSelectedPosition], then accepts it*/
    suspend fun getUserSelectedPosition():ConceptualPosition
}