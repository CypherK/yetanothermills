package ch.cypherk.yam.domain.model.game.statechanges

import ch.cypherk.yam.domain.model.base.ConceptualPosition

/**encapsulates that the [positions] form freshly closed mill(s)*/
data class MillsClosed(val positions:Collection<ConceptualPosition>):StateChange