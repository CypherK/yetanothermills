package ch.cypherk.yam.domain.threading

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.game.statechanges.StateChange
import ch.cypherk.yam.domain.model.ui.GuiActor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch

class BasicUI:GuiActor{
    private val dispatcher = Dispatchers.IO
    private val coroutineScope = BasicCoroutineScope()
    private val stateChanges = BroadcastChannel<StateChange>(capacity = 20)

    /*start a Position Actor that receives input from the UI and forwards them on demand*/
    private val requests = Channel<PositionRequest>(capacity = UNLIMITED)
    private val offeredPositions = Channel<ConceptualPosition>(capacity = UNLIMITED)
    private val nextPosition = Channel<ConceptualPosition>(capacity = UNLIMITED)
    init {
        coroutineScope.launch(dispatcher){
            positionActor(offeredPositions,requests,nextPosition)
        }
    }

    override suspend fun offerUserSelectedPosition(conceptualPosition: ConceptualPosition){
        offeredPositions.send(conceptualPosition)
    }

    override suspend fun getUserSelectedPosition(): ConceptualPosition{
        requests.send(PositionRequest.ForwardNext)
        return nextPosition.receive()
    }

    override suspend fun broadcastStateChange(change: StateChange) {
        stateChanges.send(change)
    }

    override suspend fun subscribeToStateChanges(): ReceiveChannel<StateChange> {
        return stateChanges.openSubscription()
    }
}