package ch.cypherk.yam.domain.model.mills

import ch.cypherk.yam.domain.model.base.*
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.util.withDefault

class StandardMills(private val createNode:(ConceptualPosition) -> Node = {Node(it)}):Mills {
    private val internalPositions = mutableMapOf<ConceptualPosition, Node>()
    override val position by lazy {internalPositions.toMap().withDefault {
        throw IllegalArgumentException("there's no node for position '$it'")
    }}

    init{
        //create nodes for all positions
        ConceptualPosition.values().forEach { if(it != INVALID) internalPositions[it] = createNode(it) }

        /*now build graph
        * A1 - - -  A2 - - - A3
        *  |        |         |
        *  |  B1 -- B2 -- B3  |
        *  |  |     |      |  |
        *  |  |  C1-C2-C3  |  |
        *  A4-B4-C4    C5-B5-A5
        *  |  |  C6-C7-C8  |  |
        *  |  |     |      |  |
        *  |  B6 -- B7 -- B8  |
        *  |        |         |
        *  A6 - - - A7 - - - A8
        */
        GraphBuilder.positionToNode = position

        //A-Square
        A1.right(A2)
        A1.down(A4)
        A2.right(A3)
        A2.down(B2)
        A3.down(A5)
        A4.right(B4)
        A4.down(A6)
        A5.left(B5)
        A5.down(A8)
        A6.right(A7)
        A7.up(B7)
        A7.right(A8)

        //B-Square
        B1.right(B2)
        B1.down(B4)
        B2.right(B3)
        B2.down(C2)
        B3.down(B5)
        B4.right(C4)
        B4.down(B6)
        B5.left(C5)
        B5.down(B8)
        B6.right(B7)
        B7.up(C7)
        B7.right(B8)

        //C-Square
        C1.right(C2)
        C1.down(C4)
        C2.right(C3)
        C3.down(C5)
        C4.down(C6)
        C5.down(C8)
        C6.right(C7)
        C7.right(C8)

        GraphBuilder.reset()
    }
}