package ch.cypherk.yam.domain.model.game.stage

import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.statechanges.Message
import ch.cypherk.yam.domain.model.game.statechanges.StateChange
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor
import kotlinx.coroutines.runBlocking

/**encapsulates the different stages through which a game progresses*/
interface Stage{
    /**the [Mills] being played on*/
    val board:Mills

    /**the unique [Player]s playing this stage*/
    val players:Set<Player>

    /** the [Rules] to use*/
    val rules:Rules

    /**used to communicate changes to the model to the outside world*/
    val communicator:GuiActor

    /**uses the [communicator] to broadcast a [StateChange]*/
    fun broadcast(change:StateChange) = runBlocking{ communicator.broadcastStateChange(change) }

    /**wraps the [msg] into a [Message.Info] before [broadcast]ing it*/
    fun broadcast(msg:String) = broadcast(Message.Info(msg))

    /**wraps the [msg] into a [Message.Error] before [broadcast]ing it*/
    fun broadcastError(msg:String) = broadcast(Message.Error(msg))

    /**Executes all actions for the [player]
     * @return the new [Stage] the game is in after this action*/
    fun playNextTurn(player: Player):Stage
}