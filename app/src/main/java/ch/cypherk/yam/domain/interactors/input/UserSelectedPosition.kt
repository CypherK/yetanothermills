package ch.cypherk.yam.domain.interactors.input

import ch.cypherk.yam.domain.interactors.base.UseCase
import ch.cypherk.yam.domain.interactors.base.UseCaseFailedException
import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.ui.GuiActor

/** The user selected a [ConceptualPosition] (e.g. by pressing a button)
 * If the game does not expect user input at this point in time, it is simply ignored.*/
class UserSelectedPosition(val conceptualPosition: ConceptualPosition):UseCase<Nothing?,GuiActor>{
    init {
        if(conceptualPosition == INVALID) throw IllegalArgumentException("user must not be able to select an INVALID position")
    }
    /**
     * @param onResult receives 'null', invoked after the position has been offered (even if the position was ignored)
     * @param onError invoked when the [UseCase] fails to execute
     */
    override suspend fun invoke(arg: GuiActor?, onResult: (Nothing?) -> Unit, onError: (reason: UseCaseFailedException) -> Unit) {
        arg?.let {
            arg.offerUserSelectedPosition(conceptualPosition)
            onResult(null)
        }
    }
}