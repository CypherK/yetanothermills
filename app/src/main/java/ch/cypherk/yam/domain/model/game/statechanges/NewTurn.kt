package ch.cypherk.yam.domain.model.game.statechanges

import ch.cypherk.yam.domain.model.base.PositionOwner

data class NewTurn(val player:PositionOwner):StateChange