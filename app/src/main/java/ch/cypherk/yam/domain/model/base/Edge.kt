package ch.cypherk.yam.domain.model.base



sealed class Edge{
    object None: Edge(){override fun toString() = "Edge.NONE"}
    class To(val p: Node): Edge(){override fun toString() = "Edge.To($p)"}
}