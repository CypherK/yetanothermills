package ch.cypherk.yam.domain.model.game

import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.HumanPlayer
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.DummyRules
import ch.cypherk.yam.domain.model.game.stage.DummyStage
import ch.cypherk.yam.domain.model.game.stage.Stage
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.mills.StandardMills
import ch.cypherk.yam.domain.model.ui.GuiActor
import ch.cypherk.yam.domain.threading.BasicUI
import java.util.concurrent.atomic.AtomicBoolean

class DummyGame(override val communicator: GuiActor=BasicUI(), override val mills:Mills = StandardMills()):Game{
    override val players = listOf(
            HumanPlayer(communicator,ALICE),
            HumanPlayer(communicator,BOB)
    )
    override var initialStage = DummyStage(communicator,players.toSet(),mills)
}