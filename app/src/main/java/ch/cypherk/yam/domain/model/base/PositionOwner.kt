package ch.cypherk.yam.domain.model.base

/**Players 'own' a position when they have one of their stones placed on it.
 * A position with no stone on it is owned by [NOBODY].
 * Player1 is referred to as [ALICE]; in classic mills, this would be white
 * Player2 is referred to as [BOB]; in classic mills, this would be black*/
enum class PositionOwner{
    NOBODY,ALICE,BOB
}