package ch.cypherk.yam.domain.model.game

import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.stage.GameHasEnded
import ch.cypherk.yam.domain.model.game.stage.Stage
import ch.cypherk.yam.domain.model.game.statechanges.GameEnded
import ch.cypherk.yam.domain.model.game.statechanges.Message
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor
import kotlinx.coroutines.runBlocking
import java.util.concurrent.atomic.AtomicBoolean

interface Game{
    val mills:Mills
    val initialStage:Stage
    val players:List<Player>

    val communicator:GuiActor

    /**template method:
     * first [initialiseGame],
     * [players] then play their next turn in the [activeStage] in order and
     * keep playing until the [GameHasEnded] or the game is [abort]ed,
     * at which point [whenGameEnds] is called
     * */
    fun play(){
        var activeStage = initialStage
        initialiseGame()
        var ended = false
        while(!ended) {
            for(p in players){
                activeStage = activeStage.playNextTurn(p)
            }
            ended = activeStage is GameHasEnded
        }
        whenGameEnds(activeStage)
    }

    /**sets up the game */
    fun initialiseGame(){}

    /** by default, just [broadcastWinner]*/
    fun whenGameEnds(endState:Stage){
        broadcastWinner(endState)
    }

    /**Determines the winner and broadcasts it.*/
    fun broadcastWinner(endState:Stage){
        if(endState is GameHasEnded){
            endState.winner?.let { endState.broadcast(GameEnded.WithWinner(it.conceptualPlayer)) }
            ?: endState.broadcast(GameEnded.WithoutWinner)
        }
        else endState.broadcast(GameEnded.WithoutWinner)
    }
}