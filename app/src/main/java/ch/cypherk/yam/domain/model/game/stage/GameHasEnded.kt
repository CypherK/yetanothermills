package ch.cypherk.yam.domain.model.game.stage

import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor

/**A state encapsulating an ended game.*/
class GameHasEnded(
        override val communicator: GuiActor,
        override val board: Mills,
        override val players: Set<Player>,
        override val rules:Rules,
        val winner:Player?
): Stage{

    /**there's nothing more to do, the game has ended
     * @return this*/
    override fun playNextTurn(player: Player): Stage = this
}