package ch.cypherk.yam.domain.threading

import ch.cypherk.yam.domain.interactors.base.UseCase
import ch.cypherk.yam.domain.interactors.base.UseCaseFailedException
import kotlinx.coroutines.*

/**An [Executor] using Kotlin coroutines to run [UseCase]s*/
class UseCaseExecutor:Executor,CoroutineScope {

    private val dispatcher = Dispatchers.IO
    private val job = Job()
    override val coroutineContext = job


    /**Takes a [UseCase] and the arguments for it and launches a coroutine that executes it*/
    override fun <Result, Params> invoke(
            u: UseCase<Result, Params>,
            arg: Params?,
            onResult: (Result) -> Unit,
            onError: (reason: UseCaseFailedException) -> Unit
    ) {launch(dispatcher) {
            u(arg, onResult, onError)
        }
    }
}