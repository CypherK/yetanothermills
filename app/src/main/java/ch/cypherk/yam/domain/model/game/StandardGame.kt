package ch.cypherk.yam.domain.model.game

import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.domain.model.game.player.HumanPlayer
import ch.cypherk.yam.domain.model.game.stage.standard.StageOneFreePlacements
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.mills.StandardMills
import ch.cypherk.yam.domain.model.ui.GuiActor
import ch.cypherk.yam.domain.threading.BasicUI

class StandardGame(
        override val communicator: GuiActor = BasicUI(),
        override val mills: Mills = StandardMills()
):Game{
    override val players = listOf(
            HumanPlayer(communicator, PositionOwner.ALICE),
            HumanPlayer(communicator, PositionOwner.BOB)
    )
    override val initialStage = StageOneFreePlacements(communicator,players.toSet(),mills)
}