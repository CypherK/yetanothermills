package ch.cypherk.yam.domain.model.base

/**A game of mills has 24 possible locations for a stone,
 * arrange in three concentric squares.
 *
 * We label the squares (from outer-most to innermost) A,B,C
 * and number the positions on each square independently and in row-major order.
 *
 * A1 - - -  A2 - - - A3
 *  |        |         |
 *  |  B1 -- B2 -- B3  |
 *  |  |     |      |  |
 *  |  |  C1-C2-C3  |  |
 *  A4-B4-C4    C5-B5-A5
 *  |  |  C6-C7-C8  |  |
 *  |  |     |      |  |
 *  |  B6 -- B7 -- B8  |
 *  |        |         |
 *  A6 - - - A7 - - - A8
 *
 * */
enum class ConceptualPosition{
    INVALID,
    A1,A2,A3,A4,A5,A6,A7,A8,
    B1,B2,B3,B4,B5,B6,B7,B8,
    C1,C2,C3,C4,C5,C6,C7,C8
}