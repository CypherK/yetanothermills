package ch.cypherk.yam.domain.model.game.rules

sealed class Answer{
    object Yes:Answer()
    data class  No(val reason: Reason):Answer()
}