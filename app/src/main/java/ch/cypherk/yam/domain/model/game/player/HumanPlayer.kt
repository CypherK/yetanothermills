package ch.cypherk.yam.domain.model.game.player

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.domain.model.ui.GuiActor
import kotlinx.coroutines.runBlocking

/** a player who provides input by interacting with an UI*/
class HumanPlayer(val ui:GuiActor, override val conceptualPlayer: PositionOwner):Player{
    override val name: String = conceptualPlayer.name

    override var nofStones = 0

    override fun selectPosition(): ConceptualPosition {
        return runBlocking { ui.getUserSelectedPosition() }
    }
}