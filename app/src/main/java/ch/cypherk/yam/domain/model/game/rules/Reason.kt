package ch.cypherk.yam.domain.model.game.rules

interface Reason{
    val resolution:String
}

object Initialised:Reason{
    override val resolution = "use the variable before you check it"
}