package ch.cypherk.yam.domain.model.game.rules

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.mills.Mills

/** Rules don't do anything*/
class DummyRules(override val board:Mills):Rules{
    override fun hasValidMove(player:Player):Boolean{return true}
    override fun validMoves(player: Player,from: ConceptualPosition): Set<ConceptualPosition> {return setOf()}
    override fun isValidMove(player: Player, from: ConceptualPosition, to: ConceptualPosition): Answer {return Answer.Yes}
    override fun closedMills(player: Player): Set<ConceptualPosition> {return setOf()}
    override fun closedMillsAt(pos: ConceptualPosition): Set<ConceptualPosition> {return setOf()}
    override fun playerCanRemove(player: Player, pos: ConceptualPosition): Answer {return Answer.Yes}
}