package ch.cypherk.yam.domain.model.game.statechanges

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner

/**encapsulates the fact that a position has changed ownership*/
data class OwnerHasChanged(val pos:ConceptualPosition, val newOwner:PositionOwner):StateChange