package ch.cypherk.yam.domain.model.game.stage.standard

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.base.Node
import ch.cypherk.yam.domain.model.base.PositionOwner.*
import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.Answer.*
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.rules.standard.PlayersMoveToAdjacent
import ch.cypherk.yam.domain.model.game.rules.standard.StandardRules
import ch.cypherk.yam.domain.model.game.stage.GameHasEnded
import ch.cypherk.yam.domain.model.game.stage.Stage
import ch.cypherk.yam.domain.model.game.statechanges.NewTurn
import ch.cypherk.yam.domain.model.game.statechanges.PositionHasBeenSelected
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor
import java.lang.IllegalStateException

class StageTwoMoveExisting(
        communicator: GuiActor,
        players:Set<Player>,
        board: Mills,
        rules: Rules = PlayersMoveToAdjacent(board)
    ):StandardGameStage(communicator,players,board,rules){

    override fun playNextTurn(player: Player): Stage {
        var state:State = State.TurnStart(this,player)

        while(state !is State.TurnEnd)state = state.resume()

        return if(state.gameHasEnded) GameHasEnded(communicator,board,players,rules,determineWinner())
        else this
    }

    /**@return the [Player] that has won or null if there is no winner*/
    private fun determineWinner():Player?{
        val candidates = players.filter { rules.hasValidMove(it) }
        return if(candidates.size == 1)candidates[0] else null
    }

    /**State Machine for this Stage*/
    sealed class State(protected val stage:StageTwoMoveExisting){
        val rules by lazy{stage.rules}
        abstract fun resume():State

        class TurnStart(stage:StageTwoMoveExisting, private val player:Player):State(stage){
            override fun resume(): State {
                if(!rules.hasValidMove(player)) return TurnEnd(stage,gameHasEnded = true)

                stage.broadcast(NewTurn(player.conceptualPlayer))
                return SelectingPosition(stage,player,
                        acceptSelection = {it.owner == player.conceptualPlayer},
                        whenSelectionFails = {if(it.owner != NOBODY)stage.broadcastError(StandardRules.CannotMoveOthersStones.resolution)},
                        nextState = {SelectedFrom(stage,player,it.conceptualPosition)}
                        )
            }
        }

        /**asks the [Player] to select a position until
         * @param acceptSelection receives the node corresponding to the user-selected position
         * should return true iff the selection is desirable
         *
         * @param whenSelectionFails receives the node corresponding to the user-selected position,
         * invoked when [acceptSelection] returns false
         *
         * @param nextState receives the [Node] corresponding to the selected [ConceptualPosition]
         * and constructs the state to transition to
         * @return the result of applying [nextState] to the [Node] corresponding to the selected [ConceptualPosition]*/
        class SelectingPosition(stage:StageTwoMoveExisting, private val player:Player,
                                private val acceptSelection:(Node)-> Boolean,
                                private val whenSelectionFails:(Node)->Unit,
                                private val nextState:(Node)->State
        ):State(stage){
            override fun resume(): State {
                var pos = INVALID
                var success = false
                var node:Node? = null
                while(!success){
                    pos = player.selectPosition()
                    node = stage.board.position[pos]
                    success = acceptSelection(node)
                    if(!success) whenSelectionFails(node)
                }

                assert(pos != INVALID){"player cannot select INVALID position"}
                return nextState(node!!)
            }
        }

        /**User has selected one of his stones to move.
         * Next, he'll have to either select an empty position to move to
         * or select another of his stones to move instead.*/
        class SelectedFrom(stage: StageTwoMoveExisting,private val player:Player, private val from:ConceptualPosition):State(stage){
            override fun resume(): State {
                stage.broadcast(PositionHasBeenSelected(from))
                return SelectingPosition(stage,player,
                        acceptSelection = {it.owner == NOBODY || it.owner == player.conceptualPlayer},
                        whenSelectionFails = {stage.broadcastError("You cannot make that move: ${StandardRules.PositionNotFree.resolution}")},
                        nextState = {
                            val selectedPosition = it.conceptualPosition
                            if(it.owner == NOBODY) MovingStone(stage,player,from,to=selectedPosition)
                            else SelectedFrom(stage,player,from = selectedPosition)
                        }
                )
            }
        }

        /**Will try to move from [from] to [to].
         * If succeeds, will transition to [TurnEnd], otherwise returns to [SelectedFrom].*/
        class MovingStone(stage:StageTwoMoveExisting,private val player:Player,
                          private val from:ConceptualPosition,
                          private val to:ConceptualPosition
        ):State(stage){
            override fun resume(): State {
                val didMove = stage.moveStone(player,from,to)
                return when(didMove){
                    is Yes -> TurnEnd(stage,gameHasEnded = false)
                    is No -> {
                        stage.broadcastError("You cannot make that move: ${didMove.reason.resolution}")
                        SelectedFrom(stage, player, from)
                    }
                }
            }
        }

        class TurnEnd(stage:StageTwoMoveExisting, val gameHasEnded:Boolean):State(stage){
            override fun resume() = throw IllegalStateException("cannot resume end state")
        }
    }
}