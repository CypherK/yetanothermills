package ch.cypherk.yam.domain.model.base



/**directions into which edges can extend*/
enum class MovableDirection{
    UP,DOWN,LEFT,RIGHT;
    companion object {
        /**collects inverse directions*/
        val oppositeMovableDirection = mapOf(
                UP to DOWN, DOWN to UP,
                LEFT to RIGHT, RIGHT to LEFT
        )
    }
}