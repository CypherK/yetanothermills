package ch.cypherk.yam.domain.model.game.stage

import ch.cypherk.yam.domain.model.game.player.Player
import ch.cypherk.yam.domain.model.game.rules.DummyRules
import ch.cypherk.yam.domain.model.game.rules.Rules
import ch.cypherk.yam.domain.model.game.statechanges.OwnerHasChanged
import ch.cypherk.yam.domain.model.mills.Mills
import ch.cypherk.yam.domain.model.ui.GuiActor

/**A stage stub.*/
class DummyStage(
        override val communicator: GuiActor,
        override val players:Set<Player>,
        override val board: Mills,
        override val rules: Rules = DummyRules(board)
):Stage{

    /** invokes [Player.selectPosition] on [player]
     * @return [NoChange]
     */
    override fun playNextTurn(player: Player): Stage{
        broadcast("${player.name} please select a tile")
        val pos = player.selectPosition()
        broadcast("selected $pos")
        broadcast(OwnerHasChanged(pos, player.conceptualPlayer))
        return this
    }
}