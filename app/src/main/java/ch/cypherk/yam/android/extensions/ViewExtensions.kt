package ch.cypherk.yam.android.extensions

import android.content.Context
import android.view.View

fun View.ctx(): Context = with(this){
    context
}