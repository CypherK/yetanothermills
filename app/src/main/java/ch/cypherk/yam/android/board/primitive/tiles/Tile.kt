package ch.cypherk.yam.android.board.primitive.tiles

import android.content.Context
import android.util.AttributeSet
import android.view.View
import ch.cypherk.yam.domain.model.base.PositionOwner
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

abstract class Tile : View {
    data class Coordinates(val x:Int, val y:Int)

    private var xCoord = -1
    private var yCoord = -1
    val coordinates by lazy{ Coordinates(xCoord,yCoord) }

    constructor(context: Context?, xCoord:Int, yCoord:Int):super(context){this.xCoord = xCoord;this.yCoord=yCoord}
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**some tiles can be owned by either player*/
    abstract var owner: PositionOwner

    var isHighlighted = false
        protected set(value){field = value; invalidateMe()}
    fun highlight(){isHighlighted = true}
    fun unhighlight(){isHighlighted = false}

    /**initially, this [Tile]'s [isHighlighted] is false*/
    open fun reset(){
        isHighlighted = false
    }

    /**marks this [Tile] invalid so its [onDraw] method is invoked*/
    fun invalidateMe(){
        doAsync {
            uiThread {
                invalidate()
            }
        }
    }
}