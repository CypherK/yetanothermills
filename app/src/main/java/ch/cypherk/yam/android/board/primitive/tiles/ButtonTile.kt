package ch.cypherk.yam.android.board.primitive.tiles

import android.content.Context
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import ch.cypherk.yam.R
import ch.cypherk.yam.android.board.base.InteractableElement
import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.base.PositionOwner.*

/**Tile that can be clicked. can change [owner] and changes colour if it does.*/
class ButtonTile: Tile,InteractableElement {
    var conceptualPosition = INVALID
        protected set

    constructor(context:Context?, x:Int, y:Int, conceptualPosition: ConceptualPosition):super(context,x,y){
        this.conceptualPosition = conceptualPosition
        reset()
    }
    constructor(context: Context?) : super(context){reset()}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){reset()}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){reset()}

    override var owner = NOBODY
        set(value){field=value;invalidateMe()}

    /**initially, [NOBODY] owns this tile*/
    override fun reset(){
        owner = NOBODY
        super.reset()
    }

    /**changes the [Tile]'s colour depending on its [owner] as well as on whether it [isHighlighted] or not*/
    override fun onDraw(canvas: Canvas) {
        val colorID = when(owner){
            NOBODY -> if(isHighlighted) R.color.tile_neutralHighlight else R.color.tile_neutralNormal
            ALICE -> if(isHighlighted) R.color.tile_aliceHighlight else R.color.tile_aliceNormal
            BOB -> if(isHighlighted) R.color.tile_bobHighlight else R.color.tile_bobNormal
        }

        val newColor = ContextCompat.getColor(context,colorID)
        canvas.drawColor(newColor)
    }
}