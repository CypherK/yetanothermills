package ch.cypherk.yam.android.board.base

/**an UI element that the user can interact with*/
interface InteractableElement{
    fun reset()
}