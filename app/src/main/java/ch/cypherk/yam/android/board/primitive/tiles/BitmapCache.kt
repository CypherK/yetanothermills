package ch.cypherk.yam.android.board.primitive.tiles

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import ch.cypherk.yam.R

object BitmapCache {
    data class Size(val width: Int,val height: Int)

    private var barTileSize = Size(-1,-1)
    private var barHorizontal: Bitmap? = null
    private var barVertical:Bitmap? = null

    /**
     * checks whether it has already fetched the required resource at one point and if not, caches it for later reuse
     * only the most recent size is cached
     */
    @Synchronized fun fetchBarOverlay(res: Resources, width:Int, height:Int, orientation: BarTile.Orientation):Bitmap{

        //if already exists, return that
        if(barTileSize.width == width && barTileSize.height == height){
            return existingBarBitmap(orientation)
        }

        //else refetch
        barTileSize = Size(width,height)

        //else refetch
        val imgVertical = BitmapFactory.decodeResource(res,R.drawable.bar_vertical)
        val imgHorizontal = BitmapFactory.decodeResource(res,R.drawable.bar_horizontal)

        barVertical = Bitmap.createScaledBitmap(imgVertical,width,height,true)
        barHorizontal = Bitmap.createScaledBitmap(imgHorizontal,width,height,true)

        return existingBarBitmap(orientation)
    }

    private fun existingBarBitmap(orientation: BarTile.Orientation):Bitmap{
        require(barVertical != null)
        require(barHorizontal!= null)

        return when(orientation){
            BarTile.Orientation.VERTICAL -> barVertical!!
            BarTile.Orientation.HORIZONTAL -> barHorizontal!!
            else -> throw IllegalStateException("unhandled orientation : $orientation")
        }
    }
}