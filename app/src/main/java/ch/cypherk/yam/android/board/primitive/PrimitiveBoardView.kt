package ch.cypherk.yam.android.board.primitive

import android.content.Context
import android.view.View
import android.widget.GridLayout
import ch.cypherk.yam.android.board.base.BoardView
import ch.cypherk.yam.android.board.base.InteractableElement
import ch.cypherk.yam.android.board.primitive.tiles.BarTile
import ch.cypherk.yam.android.board.primitive.tiles.ButtonTile
import ch.cypherk.yam.android.board.primitive.tiles.EmptyTile
import ch.cypherk.yam.android.board.primitive.tiles.Tile
import ch.cypherk.yam.android.board.primitive.tiles.Tile.Coordinates as coords
import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.util.withDefault

/**a board that is just a grid of buttons and non-buttons*/
class PrimitiveBoardView: BoardView {

    companion object {
        const val nofCols = 7
        const val nofRows = 7

        /**Encodes the board in a matrix.
         * |: vertical bar
         * -: horizontal bar
         * B: button
         * E: empty
         */
        val layout = arrayOf(
                charArrayOf('B','-','-','B','-','-','B'),
                charArrayOf('|','B','-','B','-','B','|'),
                charArrayOf('|','|','B','B','B','|','|'),
                charArrayOf('B','B','B','E','B','B','B'),
                charArrayOf('|','|','B','B','B','|','|'),
                charArrayOf('|','B','-','B','-','B','|'),
                charArrayOf('B','-','-','B','-','-','B')
        )
        
        /** translates [Tile.Coordinates] into [ConceptualPosition]
         *  coordinates start at (x,y) = (0,0) in the upper left corner of the [layout]
         *  the x coordinate represents position in the row
         *  the y coordinate represents the row number*/
        val conceptualPositionOf = mapOf(
                coords(0,0) to A1,
                coords(3,0) to A2,
                coords(6,0) to A3,
                coords(0,3) to A4,
                coords(6,3) to A5,
                coords(0,6) to A6,
                coords(3,6) to A7,
                coords(6,6) to A8,

                coords(1,1) to B1,
                coords(3,1) to B2,
                coords(5,1) to B3,
                coords(1,3) to B4,
                coords(5,3) to B5,
                coords(1,5) to B6,
                coords(3,5) to B7,
                coords(5,5) to B8,

                coords(2,2) to C1,
                coords(3,2) to C2,
                coords(4,2) to C3,
                coords(2,3) to C4,
                coords(4,3) to C5,
                coords(2,4) to C6,
                coords(3,4) to C7,
                coords(4,4) to C8
        ).withDefault { throw IllegalArgumentException("cannot translate $it into a conceptual position") }

        const val margin = 1
    }

    class InvalidPrimitiveBoard:IllegalArgumentException("The primitive board requires a ${nofRows}x$nofCols grid view")

    override val interactableElements = ArrayList<InteractableElement>()
    val tiles = ArrayList<ArrayList<Tile>>()

    /** takes the [layout] and translates it into UI elements */
    override fun constructIn(context:Context,view: View) {
        if(view !is GridLayout
            || (view.rowCount != nofRows || view.columnCount != nofCols)
        ) throw InvalidPrimitiveBoard()
        if(!tiles.isEmpty()) throw IllegalStateException("cannot construct a board that is already constructed")

        for(y in 0 until layout.size){
            val row = layout[y]
            val rowTiles = ArrayList<Tile>()
            tiles.add(rowTiles)

            for(x in 0 until row.size){
                val c = row[x]
                val t = when(c){
                    'B' -> {
                        val b = ButtonTile(context,x,y,conceptualPositionOf[coords(x,y)])
                        interactableElements.add(b)
                        b
                    }
                    'E' -> EmptyTile(context,x,y)
                    '-' -> BarTile(context,x,y,BarTile.Orientation.HORIZONTAL)
                    '|' -> BarTile(context,x,y,BarTile.Orientation.VERTICAL)
                    else -> throw IllegalArgumentException("unrecognised tile code '$c'")
                }
                rowTiles.add(t)
                view.addView(t)
            }
        }

        view.viewTreeObserver.addOnGlobalLayoutListener { with(view){
            //force square board
            if(height >= width) layoutParams.height = width
            else layoutParams.width = height

            //adjust tile sizes
            val itemWidth = width/columnCount
            val itemHeight = height/rowCount

            for(t in tiles.flatten()){
                val params = t.layoutParams as GridLayout.LayoutParams
                when(t){
                    is BarTile ->{
                        /*when there are multiple bar tiles in a row, they'll connect more
                          smoothly when there's no gap in-between them*/
                        params.width = itemWidth
                        params.height = itemHeight
                        params.setMargins(0,0,0,0)
                    }
                    else ->{
                        params.width = itemWidth - 2* margin
                        params.height = itemHeight - 2* margin
                        params.setMargins(margin, margin, margin, margin)
                    }
                }
                t.layoutParams = params
            }
        } }
    }
}