package ch.cypherk.yam.android.board.base

import android.content.Context
import android.view.View
import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.presentation.MillsView

/**responsible for handling UI events*/
interface BoardPresenter :MillsView {

    /**mostly for debugging purposes: get the [InteractableElement] corresponding to [pos]*/
    fun get(pos:ConceptualPosition):InteractableElement

    /** sets up the [BoardView]. that includes calling its [BoardView.constructIn] method*/
    fun constructBoardIn(context: Context, view: View)

    /** returns the underlying [InteractableElement]s so the presenter can connect everything*/
    fun interactableElements():Collection<InteractableElement>
}