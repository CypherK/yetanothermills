package ch.cypherk.yam.android.board.primitive.tiles

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import ch.cypherk.yam.R
import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.domain.model.base.PositionOwner.*

class BarTile(context: Context?, x:Int, y:Int, val orientation: Orientation) : Tile(context,x,y) {
    enum class Orientation{
        NONE,HORIZONTAL, VERTICAL
    }

    sealed class State{
        object Neutral:State()
        data class HighlightedBy(val player : PositionOwner):State()
    }

    private var state = State.Neutral
        set(value) {field = value; invalidateMe()}

    override fun reset() {
        this.state = State.Neutral
    }

    override var owner = NOBODY
        set(value){throw IllegalStateException("an bar tile cannot be owned by anybody")}

    override fun onDraw(canvas: Canvas) {
        val currentState:State = state

        //background colour
        val colorID = when(currentState){
            is State.Neutral -> R.color.tile_neutralNormal
            is State.HighlightedBy -> when(currentState.player){
                NOBODY -> R.color.tile_neutralHighlight
                ALICE -> R.color.tile_aliceHighlight
                BOB -> R.color.tile_bobHighlight
            }
        }
        val newColor = ContextCompat.getColor(context,colorID)

        //overlay, giving shape to the bar
        val overlay = BitmapCache.fetchBarOverlay(resources,width,height,orientation)

        //now draw
        canvas.drawColor(newColor)
        canvas.drawBitmap(overlay,0.toFloat(),0.toFloat(),null)
    }
}