package ch.cypherk.yam.android.board.primitive.tiles

import android.content.Context
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import ch.cypherk.yam.R
import ch.cypherk.yam.domain.model.base.PositionOwner.*

/**Represents empty spaces on the board.*/
class EmptyTile: Tile {
    constructor(context:Context?, x:Int, y:Int):super(context,x,y){reset()}
    constructor(context: Context?) : super(context){reset()}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){reset()}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){reset()}

    /**[NOBODY] owns empty space*/
    override var owner = NOBODY
        set(value){throw IllegalStateException("an empty tile cannot be owned by anybody")}

    /**changes colour of this [Tile] based on whether it [isHighlighted] or not*/
    override fun onDraw(canvas: Canvas){
        val colorID = if(isHighlighted) R.color.tile_emptyHighlight else R.color.tile_emptyNormal
        val newColor = ContextCompat.getColor(context,colorID)
        canvas.drawColor(newColor)
    }
}