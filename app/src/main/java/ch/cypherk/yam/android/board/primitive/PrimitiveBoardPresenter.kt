package ch.cypherk.yam.android.board.primitive

import android.content.Context
import android.view.View
import ch.cypherk.yam.android.board.base.BoardPresenter
import ch.cypherk.yam.android.board.base.InteractableElement
import ch.cypherk.yam.android.board.primitive.tiles.ButtonTile
import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner
import ch.cypherk.yam.util.withDefault
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PrimitiveBoardPresenter:BoardPresenter{
    private val board = PrimitiveBoardView()

    private val buttonTile = mutableMapOf<ConceptualPosition,ButtonTile>()
            .withDefault { throw IllegalArgumentException("don't know where to find button tile for $it")}

    /**
     * Constructs a board in a given view
     */
    override fun constructBoardIn(context: Context, view: View) {
        board.constructIn(context, view)
        board.interactableElements.forEach{e -> when(e){
            /*note to self: we could do even more fine-grained method mapping, if we so chose*/
            is ButtonTile -> {
                val pos = e.conceptualPosition
                buttonTile[pos] = e
            }
            else -> throw IllegalArgumentException("don't know how to handle interactable elements of type ${e.javaClass.name}")
        }}
    }

    override fun interactableElements(): Collection<InteractableElement> = board.interactableElements

    override fun highlightPosition(pos: ConceptualPosition) {doAsync { uiThread {
        buttonTile[pos].highlight()
    }}}

    override fun unhighlightPosition(pos: ConceptualPosition) {doAsync { uiThread {
        buttonTile[pos].highlight()
    }}}

    override fun unhighlightAll(){
      buttonTile.values.forEach{t -> if(t.isHighlighted)doAsync { uiThread { t.unhighlight() } }}
    }

    override fun changeOwner(pos: ConceptualPosition, newOwner: PositionOwner) {doAsync { uiThread {
        buttonTile[pos].owner = newOwner
    }}}

    override fun get(pos: ConceptualPosition): InteractableElement {
        return buttonTile[pos]
    }
}