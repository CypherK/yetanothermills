package ch.cypherk.yam.android.board.base

import android.content.Context
import android.view.View

/**responsible for displaying UI elements,
 * completely passive */
interface BoardView{
    val interactableElements:Collection<InteractableElement>
    fun constructIn(context: Context, view:View)
}