package ch.cypherk.yam.android

import android.content.Context

/** MVP view on the game **/
interface GameView{
    /**displays an info message if [clear], the [msg] will be displayed alone,
     * otherwise it'll be added as the new first line in the scrollable textview*/
    fun displayInfo(msg:String, clear:Boolean = false)

    /**displays an error message*/
    fun displayError(msg:String)

    /**gets the android view underlying the game view*/
    fun getViewContext():Context
}