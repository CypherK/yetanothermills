package ch.cypherk.yam.android

import android.content.Context
import android.view.View
import android.widget.Button
import ch.cypherk.yam.R
import ch.cypherk.yam.android.board.base.BoardPresenter
import ch.cypherk.yam.android.board.primitive.PrimitiveBoardPresenter
import ch.cypherk.yam.android.board.primitive.tiles.ButtonTile
import ch.cypherk.yam.android.board.primitive.tiles.Tile
import ch.cypherk.yam.domain.interactors.input.UserSelectedPosition
import ch.cypherk.yam.domain.model.game.DummyGame
import ch.cypherk.yam.domain.model.game.Game
import ch.cypherk.yam.domain.model.game.StandardGame
import ch.cypherk.yam.domain.model.game.stage.GameHasEnded
import ch.cypherk.yam.domain.model.game.statechanges.*
import ch.cypherk.yam.domain.threading.UseCaseExecutor
import ch.cypherk.yam.presentation.AbstractGamePresenter
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

/**MVP Presenter for the game*/
class GamePresenter(val view:GameView):AbstractGamePresenter(){
    /** responsible for the interactions with the board and the board alone*/
    val boardPresenter:BoardPresenter = PrimitiveBoardPresenter()

    /**Constructs the view in a given [View] and [Context] provided by the Activity.
     * by default, this is just delegated to the [BoardPresenter]
     */
    fun constructBoardIn(context: Context, targetView: View) {
        boardPresenter.constructBoardIn(context,targetView)
    }

    override fun startNewGame() {
        view.displayInfo("We've started a new game for you\n"
                +view.getViewContext().getString(R.string.welcome)
                ,clear = true
        )
        super.startNewGame()
    }

    override fun getNewGameInstance():Game = StandardGame()

    override fun resetBoard(game: Game){
        val ui = game.communicator
        val execute = UseCaseExecutor()
        boardPresenter.interactableElements().forEach{e -> e.reset(); when(e){
            is ButtonTile -> {
                val pos = e.conceptualPosition
                val selectPosition = UserSelectedPosition(pos)
                e.setOnClickListener { activeCoroutineScope.launch(dispatcher){execute(selectPosition,ui)}}
            }
            else -> throw IllegalArgumentException("Don't know how to reset $e")
        }}
    }

    override fun reactToStateChange(change: StateChange) {
        when (change) {
            is NewTurn -> {
                view.displayInfo("It's ${change.player}'s turn")
                boardPresenter.unhighlightAll()
            }

            is OwnerHasChanged -> {
                boardPresenter.changeOwner(change.pos, change.newOwner)
                /*val t = boardPresenter.get(change.pos) as Tile
                if(t.isHighlighted) t.unhighlight()
                else t.highlight()*/
            }

            is MillsClosed -> {
                view.displayInfo("Mills! The player can remove an enemy stone.")
                change.positions.forEach{boardPresenter.highlightPosition(it)}
            }

            is PositionHasBeenSelected -> {
                boardPresenter.unhighlightAll()
                boardPresenter.highlightPosition(change.pos)
            }
            is ValidMoveAccepted -> boardPresenter.unhighlightAll()

            is GameEnded.WithoutWinner -> view.displayInfo("GAME OVER (there was no winner)")
            is GameEnded.WithWinner -> view.displayInfo("GAME OVER (${change.winner} won)")
            is Message.Info -> view.displayInfo(change.msg)
            is Message.Error -> view.displayError(change.msg)
            else -> {} //ignore uninteresting updates
        }
    }
}