package ch.cypherk.yam.presentation

import ch.cypherk.yam.domain.model.game.statechanges.StateChange
import kotlinx.coroutines.*

interface GamePresenter{

    /**initialises a new game*/
    fun startNewGame()

    /**Starts a coroutine that listens to [stateChanges] and acts on them*/
    fun startListeningForStateChanges()

    /**stops the coroutine created by [startListeningForStateChanges]*/
    fun stopListeningForStateChanges()

    /** handles [StateChange]s received via [stateChanges]*/
    fun reactToStateChange(change: StateChange)
}