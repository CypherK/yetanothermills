package ch.cypherk.yam.presentation

import ch.cypherk.yam.domain.model.game.DummyGame
import ch.cypherk.yam.domain.model.game.Game
import ch.cypherk.yam.domain.threading.BasicCoroutineScope
import kotlinx.coroutines.*
import kotlinx.coroutines.selects.select

/**Does not implement [reactToStateChange].*/
abstract class AbstractGamePresenter(
        val dispatcher: CoroutineDispatcher = Dispatchers.IO
):GamePresenter{

    var game:Game = DummyGame()
        protected set

    var activeCoroutineScope = BasicCoroutineScope()
        protected set

    override fun startListeningForStateChanges(){
        activeCoroutineScope.launch(dispatcher){
        val stateChanges = game.communicator.subscribeToStateChanges()
        while(true){
            select<Unit> {
                stateChanges.onReceive{ reactToStateChange(it)}
            }
        }
    }}

    override fun stopListeningForStateChanges(){
        activeCoroutineScope.close()
        activeCoroutineScope = BasicCoroutineScope()
    }

    override fun startNewGame() {
        stopListeningForStateChanges()
        game = getNewGameInstance()
        resetBoard(game)
        startListeningForStateChanges()
        activeCoroutineScope.launch { game.play() }
    }

    /**creates a new instance of [Game]*/
    open fun getNewGameInstance():Game = DummyGame()

    /**receives an instance of a [Game] and resets the view to match it*/
    abstract fun resetBoard(game:Game)
}