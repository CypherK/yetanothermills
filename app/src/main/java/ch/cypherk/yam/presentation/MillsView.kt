package ch.cypherk.yam.presentation

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.PositionOwner

interface MillsView{
    /**highlights a position to indicate interactability to the user*/
    fun highlightPosition(pos: ConceptualPosition)

    /**unhighlights a position if it was highlighted*/
    fun unhighlightPosition(pos: ConceptualPosition)

    /**unhighlights all highlighted positions*/
    fun unhighlightAll()

    /**changes a position's owner*/
    fun changeOwner(pos: ConceptualPosition, newOwner: PositionOwner)
}