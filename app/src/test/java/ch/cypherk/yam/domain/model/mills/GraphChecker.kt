package ch.cypherk.yam.domain.model.mills

import ch.cypherk.yam.domain.model.base.ConceptualPosition
import ch.cypherk.yam.domain.model.base.Edge
import ch.cypherk.yam.domain.model.base.MovableDirection
import ch.cypherk.yam.domain.model.base.Node
import ch.cypherk.yam.util.withDefault

import junit.framework.AssertionFailedError


object GraphChecker{
    private val initMap = mapOf<ConceptualPosition,Node>()
            .withDefault { throw IllegalStateException("the map in GraphChecker has not been updated") }
    var positionToNode = initMap
    fun reset(){positionToNode = initMap}
}

fun ConceptualPosition.checkConnection(
        dir: MovableDirection,
        target: ConceptualPosition,
        checkInverse:Boolean,
        move:Boolean
): ConceptualPosition {
    val n = GraphChecker.positionToNode
    if(n[this].isConnectedTo(dir,target)){
        if(checkInverse){
            val oppositeDir = MovableDirection.oppositeMovableDirection[dir]?:throw AssertionFailedError(
                    "wanted to check whether $this is bidirectionally connected to $target," +
                            " but $dir cannot be reversed"
            )

            target.checkConnection(oppositeDir,this,false,false)
        }
        return if(move) target else this
    }

    else throw AssertionFailedError(
            "expected ${this.name} ${dir.name} ${target.name}," +
                    " but ${this.name} ${dir.name} ${n[this].connection[dir]}"
    )
}
fun ConceptualPosition.up(target: ConceptualPosition, checkInverse:Boolean=true, moveTo:Boolean=true) = checkConnection(MovableDirection.UP,target,checkInverse,moveTo)
fun ConceptualPosition.down(target: ConceptualPosition, checkInverse:Boolean=true, moveTo:Boolean=true) = checkConnection(MovableDirection.DOWN,target,checkInverse,moveTo)
fun ConceptualPosition.left(target: ConceptualPosition, checkInverse:Boolean=true, moveTo:Boolean=true) = checkConnection(MovableDirection.LEFT,target,checkInverse,moveTo)
fun ConceptualPosition.right(target: ConceptualPosition, checkInverse:Boolean=true, moveTo:Boolean=true) = checkConnection(MovableDirection.RIGHT,target,checkInverse,moveTo)


private fun Node.isConnectedTo(direction:MovableDirection, target:ConceptualPosition):Boolean{
    val e = connection[direction]
    return when(e){
        is Edge.None -> false
        is Edge.To -> e.p.conceptualPosition == target
    }
}