package ch.cypherk.yam.domain.model.mills

import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import org.junit.Test

class StandardMillsTest{

    @Test fun graphIsConstructedCorrectly(){
        val graph = StandardMills()
        GraphChecker.positionToNode = graph.position

        /*expected graph:
        * A1 - - -  A2 - - - A3
        *  |        |         |
        *  |  B1 -- B2 -- B3  |
        *  |  |     |      |  |
        *  |  |  C1-C2-C3  |  |
        *  A4-B4-C4    C5-B5-A5
        *  |  |  C6-C7-C8  |  |
        *  |  |     |      |  |
        *  |  B6 -- B7 -- B8  |
        *  |        |         |
        *  A6 - - - A7 - - - A8
        */

        //walk along A-Square
        A1
                .down(A4,moveTo = false)
                .right(A2)
                .down(B2,moveTo = false)
                .right(A3)
                .down(A5)
                .left(B5,moveTo = false)
                .down(A8)
                .left(A7)
                .up(B7,moveTo = false)
                .left(A6)
                .up(A4)
                .right(B4,moveTo = false)
                .up(A1)

        //walk along B-Square
        B1
                .down(B4,moveTo = false)
                .right(B2)
                .down(C2,moveTo = false)
                .right(B3)
                .down(B5)
                .left(C5,moveTo = false)
                .down(B8)
                .left(B7)
                .up(C7,moveTo = false)
                .left(B6)
                .up(B4)
                .right(C4,moveTo = false)
                .up(B1)

        C1
                .down(C4,moveTo = false)
                .right(C2)
                .right(C3)
                .down(C5)
                .down(C8)
                .left(C7)
                .left(C6)
                .up(C4)
                .up(C1)

        GraphChecker.reset()
    }
}