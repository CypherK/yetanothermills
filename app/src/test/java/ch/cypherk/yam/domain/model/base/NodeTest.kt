package ch.cypherk.yam.domain.model.base

import ch.cypherk.yam.domain.model.base.ConceptualPosition.*
import ch.cypherk.yam.domain.model.base.MovableDirection.*
import ch.cypherk.yam.domain.model.base.MovableDirection.Companion.oppositeMovableDirection
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.*
import org.junit.Test

class NodeTest{
    @Test fun connectBothWaysUP() = connectBothWays(UP)
    @Test fun connectBothWaysDOWN() = connectBothWays(DOWN)
    @Test fun connectBothWaysLEFT() = connectBothWays(LEFT)
    @Test fun connectBothWaysRIGHT() = connectBothWays(RIGHT)

    fun connectBothWays(dir:MovableDirection){
        val a = Node(A1)
        val b = Node(B1)

        a.connectBothWaysWith(b,dir)

        //connections for both have been updated
        assertEquals(1,a.connection.size)
        assertEquals(1,b.connection.size)

        //check a -> b
        a.connection[dir].let {
            assertThat(it,instanceOf(Edge.To::class.java))
            assertEquals(b,(it as Edge.To).p)
        }

        //check a <- b
        b.connection[oppositeMovableDirection[dir]].let {
            assertThat(it,instanceOf(Edge.To::class.java))
            assertEquals(a,(it as Edge.To).p)
        }
    }
}